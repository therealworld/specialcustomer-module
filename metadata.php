<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// Metadata version.

use OxidEsales\Eshop\Application\Component\UserComponent as OxUserComponent;
use OxidEsales\Eshop\Application\Controller\RegisterController as OxRegisterController;
use TheRealWorld\SpecialCustomerModule\Application\Component\UserComponent;
use TheRealWorld\SpecialCustomerModule\Application\Controller\RegisterController;
use TheRealWorld\ToolsPlugin\Core\ToolsModuleVersion;

$sMetadataVersion = '2.1';

/**
 * Module information.
 */
$aModule = [
    'id'    => 'trwspecialcustomer',
    'title' => [
        'de' => 'the-real-world - Besondere Kunden',
        'en' => 'the-real-world - Special customer',
    ],
    'description' => [
        'de' => 'Kunden können im Checkout sagen, sie sind "besonders". Der Shopbetreiber bekommt dann eine separate Mail und kann anschließend weitere Entscheidungen zum Kunden treffen.',
        'en' => 'Customers can tell you are "special" during signup. The shop merchant then receives an email and can start further manual actions.',
    ],
    'thumbnail' => 'picture.png',
    'version'   => ToolsModuleVersion::getModuleVersion('trwspecialcustomer'),
    'author'    => 'Mario Lorenz',
    'url'       => 'https://www.the-real-world.de',
    'email'     => 'mario_lorenz@the-real-world.de',
    'events'    => [
        'onActivate'   => '\TheRealWorld\SpecialCustomerModule\Core\SpecialCustomerEvents::onActivate',
        'onDeactivate' => '\TheRealWorld\SpecialCustomerModule\Core\SpecialCustomerEvents::onDeactivate',
    ],
    'extend' => [
        // Component
        OxUserComponent::class => UserComponent::class,
        // Controller
        OxRegisterController::class => RegisterController::class,
    ],
    'controllers' => [
    ],
    'blocks' => [
        [
            'template' => 'form/fieldset/user_account.tpl',
            'block'    => 'user_account_newsletter',
            'file'     => 'Application/views/blocks/user_account_newsletter.tpl',
        ],
        [
            'template' => 'form/fieldset/user_noaccount.tpl',
            'block'    => 'user_noaccount_newsletter',
            'file'     => 'Application/views/blocks/user_noaccount_newsletter.tpl',
        ],
    ],
    'templates' => [
        'trwspecialcustomer_checkout.tpl' => 'trw/trwspecialcustomer/Application/views/frontend/tpl/trwspecialcustomer_checkout.tpl',
    ],
    'settings' => [
        [
            'group' => 'trwspecialcustomer_option',
            'name'  => 'boolTRWSpecialCustomerActive',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwspecialcustomer_desc',
            'name'  => 'aarrTRWSpecialCustomerCheckbox',
            'type'  => 'aarr',
            'value' => [
                'de' => 'Ich bin ein besonderer Kunde, bitte aktivieren Sie weitere Rechte',
                'en' => 'I am a special customer, please enable additional rights',
            ],
        ],
        [
            'group' => 'trwspecialcustomer_desc',
            'name'  => 'aarrTRWSpecialCustomerCheckboxHelp',
            'type'  => 'aarr',
            'value' => [
                'de' => 'Der Shopbetreiber wird per E-Mail informiert und meldet sich bei Ihnen.',
                'en' => 'The shop merchant will be informed by e-mail and will contact you.',
            ],
        ],
        [
            'group' => 'trwspecialcustomer_desc',
            'name'  => 'aarrTRWSpecialCustomerEMailSubject',
            'type'  => 'aarr',
            'value' => [
                'de' => 'Der Kunde mit der E-Mailadresse %s bittet um weitere Rechte.',
                'en' => 'The customer with the email address %s is requesting additional rights.',
            ],
        ],
        [
            'group' => 'trwspecialcustomer_desc',
            'name'  => 'aarrTRWSpecialCustomerEMailText',
            'type'  => 'aarr',
            'value' => [
                'de' => 'Der Kunde mit der E-Mailadresse %s bittet um weitere Rechte.',
                'en' => 'The customer with the email address %s is requesting additional rights.',
            ],
        ],
    ],
];
