<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

/**
 * @author    Mario Lorenz, www.the-real-world.de
 * @copyright 2020 the-real-world.de
 * @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

namespace TheRealWorld\SpecialCustomerModule\Core;

use OxidEsales\Eshop\Core\Registry;

class SpecialCustomerHelper
{
    /** Helper to collect Texts from Config */
    public static function getSpecialCustomerConfigText(string $sConfigKey, string $replace = ''): string
    {
        $langAbbr = Registry::getLang()->getLanguageAbbr();
        $aText = (array) Registry::getConfig()->getConfigParam($sConfigKey);
        $sText = $aText[$langAbbr] ?? '';
        if ($replace) {
            $sText = sprintf($sText, $replace);
        }

        return $sText;
    }
}
