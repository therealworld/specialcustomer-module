<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SpecialCustomerModule\Application\Controller;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\SpecialCustomerModule\Core\SpecialCustomerHelper;

class RegisterController extends RegisterController_parent
{
    /** Template getter show Special Customer Check? */
    public function isSpecialCustomerCheck(): bool
    {
        return (bool) Registry::getConfig()->getConfigParam('boolTRWSpecialCustomerActive');
    }

    /** Template getter Special Customer Checkbox-Text */
    public function getSpecialCustomerCheckboxText(): string
    {
        return SpecialCustomerHelper::getSpecialCustomerConfigText('aarrTRWSpecialCustomerCheckbox');
    }

    /** Template getter Special Customer Checkbox-Helptext */
    public function getSpecialCustomerCheckboxHelp(): string
    {
        return SpecialCustomerHelper::getSpecialCustomerConfigText('aarrTRWSpecialCustomerCheckboxHelp');
    }
}
