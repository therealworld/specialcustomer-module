[{if $oView|method_exists:'isSpecialCustomerCheck' && $oView->isSpecialCustomerCheck()}]
    <div class="form-group row">
        <div class="col-lg-9 offset-lg-3">
            <input type="hidden" name="btrwspecialcustomer" value="0">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="btrwspecialcustomer" value="1"> [{$oView->getSpecialCustomerCheckboxText()}]
                </label>
            </div>
            [{if $oView->getSpecialCustomerCheckboxHelp()}]
                <span class="help-block">[{$oView->getSpecialCustomerCheckboxHelp()}]</span>
            [{/if}]
        </div>
    </div>
[{/if}]
