<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------

$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwspecialcustomer_option' => 'Optionen',
    'SHOP_MODULE_GROUP_trwspecialcustomer_desc'   => 'Textbausteine',

    'SHOP_MODULE_boolTRWSpecialCustomerActive'            => 'Aktiviere zusätzliche Checkbox im Anmeldeprozess',
    'HELP_SHOP_MODULE_boolTRWSpecialCustomerActive'       => 'Beim Anlegen eines Kundenkontos oder einer Gastbestellung gibt es eine weitere Checkbox',
    'SHOP_MODULE_aarrTRWSpecialCustomerCheckbox'          => 'Text für die Checkbox im Anmeldeprozess',
    'HELP_SHOP_MODULE_aarrTRWSpecialCustomerCheckbox'     => 'Bitte lege für jede aktive Sprache einen passenden Text an (z.B. de => Ich bin ein besonderer Kunde, bitte aktivieren Sie weitere Rechte)',
    'SHOP_MODULE_aarrTRWSpecialCustomerCheckboxHelp'      => 'Text für die Hilfetext an der Checkbox im Anmeldeprozess',
    'HELP_SHOP_MODULE_aarrTRWSpecialCustomerCheckboxHelp' => 'Bitte lege für jede aktive Sprache einen passenden Text an (z.B. de => Der Shopbetreiber wird per E-Mail informiert und meldet sich bei Ihnen)',
    'SHOP_MODULE_aarrTRWSpecialCustomerEMailSubject'      => 'Text für Benchrichtigungs-Email - Betreff',
    'HELP_SHOP_MODULE_aarrTRWSpecialCustomerEMailSubject' => 'Bitte lege für jede aktive Sprache einen passenden Text an. %s ist der Platzhalter für die E-Mail-Adresse (z.B. de => Der Kunde mit der E-Mailadresse %s bittet um weitere Rechte.)',
    'SHOP_MODULE_aarrTRWSpecialCustomerEMailText'         => 'Text für Benchrichtigungs-Email - Text',
    'HELP_SHOP_MODULE_aarrTRWSpecialCustomerEMailText'    => 'Bitte lege für jede aktive Sprache einen passenden Text an. %s ist der Platzhalter für die E-Mail-Adresse (z.B. de => Der Kunde mit der E-Mailadresse %s bittet um weitere Rechte.)',
];
