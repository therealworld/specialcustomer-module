<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------

$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwspecialcustomer_option' => 'Options',
    'SHOP_MODULE_GROUP_trwspecialcustomer_desc'   => 'Text passage',

    'SHOP_MODULE_boolTRWSpecialCustomerActive'            => 'Activate additional checkbox in the checkout',
    'HELP_SHOP_MODULE_boolTRWSpecialCustomerActive'       => 'When creating a customer account or a guest order, there is another checkbox',
    'SHOP_MODULE_aarrTRWSpecialCustomerCheckbox'          => 'Text for the checkbox in the registration process',
    'HELP_SHOP_MODULE_aarrTRWSpecialCustomerCheckbox'     => 'Please create a suitable text for each active language (e.g. de => I am a special customer, please activate further rights)',
    'SHOP_MODULE_aarrTRWSpecialCustomerCheckboxHelp'      => 'Text for the help text on the checkbox in the registration process',
    'HELP_SHOP_MODULE_aarrTRWSpecialCustomerCheckboxHelp' => 'Please create a suitable text for each active language (e.g. de => The shop operator will be informed by e-mail and will contact you)',
    'SHOP_MODULE_aarrTRWSpecialCustomerEMailSubject'      => 'Text for notification email - Subject',
    'HELP_SHOP_MODULE_aarrTRWSpecialCustomerEMailSubject' => 'Please create a suitable text for each active language. %s is the placeholder for the email address (e.g. de => The customer with the email address %s is asking for additional rights.)',
    'SHOP_MODULE_aarrTRWSpecialCustomerEMailText'         => 'Text for notification email - Text',
    'HELP_SHOP_MODULE_aarrTRWSpecialCustomerEMailText'    => 'Please create a suitable text for each active language. %s is the placeholder for the email address (e.g. de => The customer with the email address %s is asking for additional rights.)',
];
