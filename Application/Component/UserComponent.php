<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SpecialCustomerModule\Application\Component;

use OxidEsales\Eshop\Core\Email;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\SpecialCustomerModule\Core\SpecialCustomerHelper;
use TheRealWorld\ToolsPlugin\Core\ToolsString;

class UserComponent extends UserComponent_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected function _afterLogin($oUser)
    {
        $result = parent::_afterLogin($oUser);
        $this->sendCustomerSpecialMail();

        return $result;
    }

    protected function sendCustomerSpecialMail(): bool
    {
        $result = false;
        $isSpecialCustomer = (bool) Registry::getRequest()->getRequestParameter('btrwspecialcustomer');
        $mail = (string) $this->getUser()->getTRWStringData('oxusername');
        if ($mail && $isSpecialCustomer) {
            $subject = SpecialCustomerHelper::getSpecialCustomerConfigText('aarrTRWSpecialCustomerEMailSubject', $mail);
            $subject = ToolsString::convertHtmlToText($subject);
            $body = SpecialCustomerHelper::getSpecialCustomerConfigText('aarrTRWSpecialCustomerEMailText', $mail);
            $oEmail = oxNew(Email::class);
            $result = $oEmail->sendContactMail($mail, $subject, $body);
        }

        return $result;
    }
}
